#!/bin/bash

set -e

SRCDIR=$(dirname $(dirname $(realpath "${BASH_SOURCE[0]}")))
DOCKER_USER=$(docker info 2>/dev/null | grep Username | awk '{print $2}')
DOCKER_USER=${DOCKER_USER:-${USER}}

SESSION_NAME=$(basename ${BASH_SOURCE[0]})
SESSION_NAME=${SESSION_NAME#*.}
SESSION_CONF="${SRCDIR}/sessions.d/${SESSION_NAME}.conf"
if [ -z "${SESSION_NAME}" -o ! -f "${SESSION_CONF}" ]; then
  echo "${BASH_SOURCE[0]}: no such session '${SESSION_NAME}' as ${SESSION_CONF}" >&2;
  exit 1;
fi

# Known conf options:
#
# CLI_DEFAULT_SUITE: string
# CLI_DEFAULT_ARCH: string
# CLI_ALL_SUITES_ARCHS: pairs of <suite>[:<arch>]
#
# DOCKER_BASE_IMAGE: string, mandatory
# DOCKER_BUILD_HAS_ARCH: strings, default: true
DOCKER_BUILD_HAS_ARCH=true
# DOCKER_BUILD_ADDITIONAL_PKGS: strings
# DOCKER_BUILD_OPTIONAL_PKGS: strings
# USER_SUPP_GROUPS: comma separated group names
#
# DOCKER_RUN_NO_DETACHED: boolean, default false
# DOCKER_RUN_NO_FREEDNS: boolean, default false
# DOCKER_RUN_NO_TTY: boolean, default false
# DOCKER_RUN_NO_INIT: boolean, default false
# DOCKER_RUN_RESTART_POLICY: string, default "always"
DOCKER_RUN_RESTART_POLICY=always
# DOCKER_RUN_OPTS: string
# DOCKER_RUN_PRIVILEGED: boolean, default false
# DOCKER_RUN_VOLUMES: docker volumes, default: /home, /tmp, /srv
# DOCKER_RUN_WORKDIR: path

source "${SESSION_CONF}"

CLI_ALL_SUITES_ARCHS=${CLI_ALL_SUITES_ARCHS:-bionic focal}
DOCKER_RUN_VOLUMES=${DOCKER_RUN_VOLUMES:-/home:/home /tmp:/tmp /srv:/srv /opt:/opt}
test "${DOCKER_BUILD_HAS_ARCH}" = "true" || DOCKER_BUILD_HAS_ARCH=
DOCKER_BUILD_COMMON_PKGS="aptitude bash-completion less sudo time vim"

arg_command=status
arg_suite=${CLI_DEFAULT_SUITE:-bionic}
arg_arch=${CLI_DEFAULT_ARCH:-amd64}
arg_image_name=
arg_container_name=
arg_rebuild=
arg_pull=--pull

while [[ $# -gt 0 ]]; do
  case "$1" in
  --image-name*|--container-name*)
    name=${1%%=*}
    name="arg_$(echo "${name#--}" | tr '-' '_')"
    declare -n arg=$name
    if [ "${1#*=}" != "$1" ]; then
      arg=${1#*=}
    elif [ $# -gt 1 ]; then
      shift;
      arg="$1"
    else
      echo "Missing argument for '$1'. Stop." >&2
      exit 1
    fi
    shift;;
  --no-pull)
    arg_pull=
    shift;;
  --pull)
    arg_pull=--pull
    shift;;
  --rebuild)
    arg_rebuild=true
    shift;;
  *)
    break;;
  esac
done

if [ $# -gt 0 ]; then arg_command=$1; shift; fi
if [ $# -gt 0 ]; then arg_suite=$1; shift; fi
if [ $# -gt 0 ]; then arg_arch=$1; shift; fi

build_docker ()
{
  iname=${arg_image_name:-${DOCKER_USER}/local:${SESSION_NAME}_${1}${DOCKER_BUILD_HAS_ARCH:+_${2}}}
  uid=$(id -u "${USER}")

  if docker inspect "${iname}" >/dev/null 2>&1 && [ "${arg_rebuild}" != "true" ]; then
    echo "Skip already built $iname ..."
    return
  fi

  echo "Building $iname ..."
  cat <<EOF |
FROM ${DOCKER_BASE_IMAGE}:${1}${DOCKER_BUILD_HAS_ARCH:+-${2}}

RUN set -e; \
	export DEBIAN_FRONTEND=noninteractive; \
	apt-get update --quiet --quiet; \
	apt-get upgrade --yes; \
	apt-get install --no-install-recommends --yes \
		${DOCKER_BUILD_COMMON_PKGS} \
		${DOCKER_BUILD_ADDITIONAL_PKGS}; \
	for pkg in ${DOCKER_BUILD_OPTIONAL_PKGS}; do \
		if [ -n "\$(apt policy \$pkg 2>/dev/null)" ]; then \
			apt-get install --no-install-recommends --yes \$pkg; \
		fi; \
	done

RUN if getent passwd ${uid}; then \
		userdel --force --remove "\$(getent passwd ${uid} | cut -d: -f1)"; \
	fi

RUN useradd --comment 'Local Development Account' \
		--home ${HOME} --no-create-home \
		--shell /bin/bash \
		--uid ${uid} \
		${USER_SUPP_GROUPS:+ --groups ${USER_SUPP_GROUPS}} \
		${USER} \
	&& (echo "${USER}:${USER}" | chpasswd) \
	&& adduser ${USER} sudo \
	&& (echo "${USER} ALL=NOPASSWD: ALL" > /etc/sudoers.d/${USER}) \
	&& chmod 0440 /etc/sudoers.d/${USER}

WORKDIR ${HOME}
User ${USER}
EOF
    docker build --no-cache ${arg_pull} -t "${iname}" -
}

run_docker ()
{
  iname=${arg_image_name:-${DOCKER_USER}/local:${SESSION_NAME}_${1}${DOCKER_BUILD_HAS_ARCH:+_${2}}}
  cname=${arg_container_name:-${DOCKER_USER}_${SESSION_NAME}_${1}${DOCKER_BUILD_HAS_ARCH:+_${2}}}
  docker run \
      ${DOCKER_RUN_PRIVILEGED:+ --privileged} \
      ${DOCKER_RUN_NO_DETACHED:- --detach} \
      ${DOCKER_RUN_NO_TTY:- --tty} \
      ${DOCKER_RUN_NO_FREEDNS:- --dns 8.8.8.8} \
      ${DOCKER_RUN_NO_INIT:- --init} \
      ${DOCKER_RUN_RESTART_POLICY:+ --restart="${DOCKER_RUN_RESTART_POLICY}"} \
      $(for v in ${DOCKER_RUN_VOLUMES}; do echo -n " --volume $v"; done) \
      ${DOCKER_RUN_WORKDIR:+ --workdir ${DOCKER_RUN_WORKDIR}} \
      --name=${cname} \
      ${DOCKER_RUN_OPTS} \
      ${iname} \
      /bin/bash
}

stop_docker ()
{
  cname=${arg_container_name:-${DOCKER_USER}_${SESSION_NAME}_${1}${DOCKER_BUILD_HAS_ARCH:+_${2}}}
  echo "Stopping ${cname} ..."
  docker rm -f ${cname}
}

status_docker ()
{
  cname=${arg_container_name:-${DOCKER_USER}_${SESSION_NAME}_${1}${DOCKER_BUILD_HAS_ARCH:+_${2}}}
  docker inspect ${cname}
}

do_command ()
{
  case "$1" in
    build|run|stop|status) ${1}_docker $2 $3; ;;
    *) echo "Don't know what to do with command '$1'. Exit."; exit 1; ;;
  esac
}

if [ "x${arg_suite}" = "xall" ]; then
  for sa in ${CLI_ALL_SUITES_ARCHS}; do
    suite=${sa%%:*}
    arch=${sa#${suite}}
    arch=${arch#:}
    arch=${arch:-${arg_arch}}
    do_command ${arg_command} ${suite} ${arch} || true
  done
else
  do_command ${arg_command} ${arg_suite} ${arg_arch};
fi
